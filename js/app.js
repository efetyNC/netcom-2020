jQuery(document).ready(function ($) {

    /*** VARIABLES GENERALES ***/

    var $window = $(window);
    var $document = $(document);
    var megamenu = $('.megamenu');
    var burger = $('.burger');
    var body = $('body');
    var footer = $('footer');
    var niveau1_li = $('.menu-desktop > li');
    var niveau1_a = $('.menu-desktop > li > a');
    var burger_ul = $('.menu-mobile ul');
    var burger_ul_3e_nv = $('.menu-mobile ul ul');
    var burger_header = $('.burger-header');

    /*** FIN VARIABLES GENERALES ***/

    /*** UTILITIES ***/

    //ScrollTo
    function scrollTo(target, condition) {
        if (condition.length) {
            var scroll = target.offset().top - $('header').height(); // prise en compte du header sticky - si sticky partiel, mettre la partie du header qui sera sticky à la place
            $("html, body").stop().animate({scrollTop: scroll}, 1500);
        }
    }
    /*** Fin UTILITIES ***/

    /*** GLOBAL ***/

    // Texte tapé
    let typedHome = new Typed('#home .type-wrap .string', {
        strings: ["Pour le <br><b>secteur public</b>"],
        typeSpeed: 50,
        backSpeed: 50,
        backDelay: 4000,
        smartBackspace: false,
        loop: true,
        cursorChar: '_',
    });
    let typedHome2 = new Typed('#home2 .type-wrap .string', {
        strings: ["Tout au long <br><b>du projet</b>"],
        typeSpeed: 50,
        backSpeed: 50,
        backDelay: 4000,
        smartBackspace: false,
        loop: true,
        cursorChar: '_',
    });
    let typedReferences = new Typed('#references .type-wrap .string', {
        strings: ["Nos <b>références</b>"],
        typeSpeed: 50,
        backSpeed: 50,
        backDelay: 4000,
        smartBackspace: false,
        loop: true,
        cursorChar: '_',
    });
    let typedTemoignages = new Typed('#temoignages .type-wrap .string', {
        strings: ["Votre <b>satisfaction</b>"],
        typeSpeed: 50,
        backSpeed: 50,
        backDelay: 4000,
        smartBackspace: false,
        loop: true,
        cursorChar: '_',
    });
    let typedEngage = new Typed('#nous-nous-engageons .type-wrap .string', {
        strings: ["<b>engagée</b>"],
        typeSpeed: 50,
        backSpeed: 50,
        backDelay: 4000,
        smartBackspace: false,
        loop: true,
        cursorChar: '_',
    });

    // owl carousel - références
    $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay: false,
        autoplayTimeout: 2500,
        dots: false,
        nav:true,
        touchDrag: true,
        mouseDrag: true,
        items: 6,
        responsive: {
            0: {
                items: 2,
                autoplay: true
            },
            900: {
                items: 6
            }
        }
    });

    // Filtres - Références
    $('.filter a').on('click', function(){
        $('.filter a').removeClass('active');
        $(this).addClass('active');
       let filtre = 'references-'+$(this).attr('data-type');
       let conteneurRef = $('.references-container');
        conteneurRef.removeClass('active');
        conteneurRef.each(function(){
            if($(this).hasClass(filtre)) {
                $(this).addClass('active');
            }
        });
    });

    $('.filter .close').on('click', function(){
        $('.filter').removeClass('open');
    });

    // Swiper carousel - témoignages
    const swiperTemoignages = new Swiper('#temoignages .swiper-container', {
        pagination: {
            el: '#temoignages .swiper-pagination',
            clickable: true
        },
        navigation: {
            prevEl: '#temoignages .swiper-button-prev',
            nextEl:'#temoignages .swiper-button-next'
        },
        autoplay: {
            delay: 5000,
        },
        spaceBetween: 0,
        centeredSlides: true,
        slidesPerView: 1,
        loop: true,
    });
    // Swiper carousel - Engagements
    const swiperEngagements = new Swiper('#nous-nous-engageons .swiper-container', {
        pagination: {
            el: '#nous-nous-engageons .swiper-pagination',
            clickable: true
        },
        navigation: {
            prevEl: '#nous-nous-engageons .swiper-button-prev',
            nextEl:'#nous-nous-engageons .swiper-button-next'
        },
        autoplay: {
            delay: 5000,
        },
        spaceBetween: 0,
        slidesPerView: 'auto',
        loop: true,
        loopedSlides: 4,
    });




    let random = Math.floor(Math.random() * 6) + 1;
    $('body').addClass('home'+random);

    // tableau responsive
    $('.wysiwyg table, .main-content table').wrap('<div class="tableau-responsive container"></div>');

    var navhei = $('.conteneur-logo').height();
    var navheix = navhei + 70;
    document.addEventListener('invalid', function (e) {
        $(e.target).addClass("invalid");

        $('html, body').stop().animate({
            scrollTop: ($($(".invalid")[0]).offset().top - navheix)
        }, 300);

        setTimeout(function () {
            $('.invalid').removeClass('invalid');
        }, 300);
    }, true);


    /*** FIN GLOBAL ***/

    /*** HEADER ***/

    const win = $(window);
    let header = $('#navigation');
    let winH;
    let lastScroll = 0;

    let elementScroll = header.offset().top;
    scroll = win.scrollTop();
    winH = win.innerHeight();
    className = 'top';
    if (scroll > 0) {
        className = 'home scroll';
    }
    if (scroll > 0 && scroll < winH) {
        className = "home";
    }
    header.attr('class',className);

    win.scroll((event) => {
        let elementScroll = header.offset().top;

        scroll = win.scrollTop();
        winH = win.innerHeight();

        className = 'top';

        if (scroll > 0) {
            className = 'home scroll';
        }

        if (scroll > 0 && scroll < winH) {
            className = "home";
        }

        if (lastScroll > scroll) {
            className = className + ' toTop';
        } else {
            className = className + ' toBottom';
        }

        $('#navigation').attr('class',className);
        lastScroll = scroll;
    });

    if($(window).width() < 1023) {
        $('#navigation nav').insertBefore('#navigation').addClass('mobile');
        if(!$('nav.mobile .contentmenu').length > 0) {
            setTimeout(function(){
                $('nav.mobile #menu-wrapper').wrap('<div class="contentmenu"></div>');
            },100);
        }
    }

    $('#burger').on('click', function(){
       $(this).toggleClass('open');
       if($('nav.mobile').hasClass('open')) {
           $('nav.mobile').removeClass('open').addClass('close');
       } else {
           $('nav.mobile').addClass('open').removeClass('close');
       }
    });

    $('nav.mobile .history').on('click', function(){
        $('#burger').removeClass('open');
        $('nav.mobile').removeClass('open').addClass('close');
    });

    $(window).on('resize', function(){
        if($(window).width() < 1023) {
            $('#navigation nav').insertBefore('#navigation').addClass('mobile');
            if(!$('nav.mobile .contentmenu').length > 0) {
                $('nav.mobile #menu-wrapper').wrap('<div class="contentmenu"></div>');
            }
        } else {
            $('nav.mobile').insertAfter('#logotop').removeClass('mobile');
            $('#menu-wrapper').appendTo('#navigation nav');
            $('.contentmenu').remove();

        }
    });

    /*** FIN HEADER ***/

    // Add smooth scrolling to all links
    $(".anchor").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 70
            }, 800, function () {
            });
        }
    });

    $('.selectfilter').on('click', function(){
        if($(window).width() < 901) {
            $('.filter').toggleClass('open');
            $('.filtercontent br').each(function(){
                $(this).replaceWith('<span class="space"></span>')
            });
        }
    });

    $('.filter li a').on('click', function(){
        if($(window).width() < 901) {
            let filter = $(this).html();
            $('.filter').removeClass('open');
            $('.selectfilter').html(filter);
        }
    });

    $(window).on('resize', function(){
       if($(window).width() < 901) {
            $('.filtercontent br').each(function(){
               $(this).replaceWith('<span class="space"></span>')
            });
            $('.references-container.references-all')
       } else {
           $('.filtercontent .space').each(function(){
               $(this).replaceWith('<br>');
           });
       }
    });

    // $('.showslide').on('click', function(){
    //    var target = $(this).attr('href');
    //    var etude = '.etude-'+$(this).attr('data-ref');
    //     $(target).addClass('activating');
    //     $(etude).addClass('active');
    //     setTimeout(function(){
    //         $(target).addClass('active');
    //    },50);
    //     $('body').addClass('no-scroll');
    //     $('body').addClass('etudes-show');
    // });


    $('.showslide').on('click', function(){
        var target = $(this).attr('href');
        var etude = $(this).attr('data-ref');
        $(target).addClass('activating');
        if(etude === 'artisanat') {
            $('.popup-references .conteneur-etudes').html(etudeartisanat);
        } else if(etude === 'tours') {
            $('.popup-references .conteneur-etudes').html(etudetours);
        } else if(etude === 'inhesj') {
            $('.popup-references .conteneur-etudes').html(etudeinhesj);
        } else if(etude === 'mlp') {
            $('.popup-references .conteneur-etudes').html(etudemlp);
        } else if(etude === 'pdch') {
            $('.popup-references .conteneur-etudes').html(etudepdch);
        } else if(etude === 'bretagne') {
            $('.popup-references .conteneur-etudes').html(etudebretagne);
        } else if(etude === 'ipparis') {
            $('.popup-references .conteneur-etudes').html(etudeipparis);
        }
        setTimeout(function(){
            $(target).addClass('active');
        },50);
        $('body').addClass('no-scroll');
        $('body').addClass('etudes-show');

        $('.popup-references .gotocontact').on('click', function(){
            $('body').removeClass('etudes-show');
            $('.popup-references').removeClass('active');
            setTimeout(function(){
                $('.popup-contact').addClass('open');
            },50);
            setTimeout(function(){
                $('.popup-references .conteneur-etudes .etude').remove();
            },500);
            $('.popup-contact').addClass('opening');
            var mymap = L.map('carte-netcom', {
                center: [48.393434822095, 2.982704559498983],
                zoom: 5,
                scrollWheelZoom: false
            });
            L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
                subdomains: 'abcd',
                maxZoom: 19
            }).addTo(mymap);
            var myIcon = L.icon({
                iconUrl: 'img/pictos/marker.png',
                iconSize: [81, 101],
                iconAnchor: [40, 70],
                // popupAnchor: [-3, -76],
            });
            var marker = L.marker([48.21414166229516, 3.277865127312694], {icon: myIcon}).addTo(mymap);


        });

    });


    // $('.popup-references .close').on('click', function(){
    //     $('body').removeClass('etudes-show');
    //     $('body').removeClass('no-scroll');
    //     $('.popup-references').removeClass('active');
    //     setTimeout(function(){
    //         $('.etude').removeClass('active');
    //     },500);
    // });

    $('.popup-references .close').on('click', function(){
        $('body').removeClass('etudes-show');
        $('body').removeClass('no-scroll');
        $('.popup-references').removeClass('active');
        setTimeout(function(){
            $('.popup-references .conteneur-etudes .etude').remove();
        },500);
    });





    $('a[href="#contact-top"]').on('click', function(){
        let ancre = $(this).attr('data-ancre');
        $('.popup-contact .close').attr('href',ancre);
        $('.popup-contact').addClass('opening');
        setTimeout(function(){
            $('.popup-contact').addClass('open');
        },50);
        $('body').addClass('no-scroll');
        var mymap = L.map('carte-netcom', {
            center: [48.393434822095, 2.982704559498983],
            zoom: 5,
            scrollWheelZoom: false
        });
        L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            subdomains: 'abcd',
            maxZoom: 19
        }).addTo(mymap);
        var myIcon = L.icon({
            iconUrl: 'img/pictos/marker.png',
            iconSize: [81, 101],
            iconAnchor: [40, 70],
            // popupAnchor: [-3, -76],
        });
        var marker = L.marker([48.21414166229516, 3.277865127312694], {icon: myIcon}).addTo(mymap);
    });

    $('.popup-contact .close').on('click', function(){
        $('body').removeClass('no-scroll');
        $('.popup-contact').removeClass('open');
    });

    $('[data-fancybox="gallery"]').fancybox({
        buttons: [
            "close"
        ],
        clickContent    : false
    });



    /*** SLICKS ****/
    // mettez ici tous les slicks du site

    $('.references-mobile').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    });
    $('.slider-svg-mobile').slick({
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        autoplay: 4000,
        slidesToShow: 1,
        slidesToScroll: 1,
    });


    // on load (ne pas oublier le resize)
	setTimeout(function(){
		if($(window).width() < 901){
			$('#tweets .tweets-container').slick({
				dots: true,
				arrows: false,
				infinite: true,
				slidesToScroll: 1,
				slidesToShow: 1,
				autoplay: 4000,
				speed: 300,
			});
		}
		if($(window).width() > 900){
			$('.tweets-container').masonry({
				// options
				itemSelector: '.tweet',
			});
		}
	}, 1000);

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function(){
        if($(window).width() < 901){
            $('.tweets-container').masonry('destroy');
            $('#tweets .tweets-container').slick({
                dots: true,
                arrows: false,
                infinite: true,
                slidesToScroll: 1,
                slidesToShow: 1,
                autoplay: 4000,
                speed: 300,
            });
        }
        if($(window).width() > 900){
            if($('#tweets .tweets-container').hasClass('slick-initialized')){
                $('#tweets .tweets-container').slick('unslick');
            }
			$('.tweets-container').masonry({
				// options
				itemSelector: '.tweet',
			});
        }
    });
});

let etudeartisanat = '<div class="etude etude-artisanat">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/artisanat/logo.png" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">Chambres de Métiers et de l\'artisanat</h2>\n' +
    '                            <a href="https://www.artisanat.fr/" target="_blank" class="btn btn-etude">Voir le site\n' +
    '                                internet <i class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-md-6 offset-md-6 offset-xl-7">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p><strong>CMA France</strong> (anciennement APCMA) est l’établissement national\n' +
    '                                        fédérateur du réseau des chambres de métiers et de l’artisanat françaises.\n' +
    '                                        Il s\'assure que l\'artisanat garde un <strong>rôle économique de premier ordre et\n' +
    '                                            défend les intérêts de la branche professionnelle.</strong></p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-5 conteneur-longpage">\n' +
    '                                <img src="img/etudes/artisanat/longpage.jpg" alt="" class="longpage">\n' +
    '                            </div>\n' +
    '                            <div class="col-xl-5 col-md-6 offset-md-1 offset-xl-2">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p>Le portail <strong>artisanat.fr</strong> est <strong>le pilier de la\n' +
    '                                            communication institutionnelle</strong> de CMA France mais également comme\n' +
    '                                            <strong>la porte d\'entrée vers l\'ensemble des offres de services</strong>\n' +
    '                                            proposées par chaque CMA en région.</p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <img src="img/etudes/artisanat/tablette.png" alt="" class="tablette">\n' +
    '                    <img src="img/etudes/artisanat/tablette-mobile.png" alt="" class="tablette-mobile">\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-7 offset-xl-5">\n' +
    '                                <h3>Notre intervention</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <ul>\n' +
    '                                        <li><strong>Analyse et restructuration de l\'information</strong></li>\n' +
    '                                        <li><strong>Expertise ergonomique/UX - Navigation orientée utilisateurs</strong>\n' +
    '                                            (entrées par typologies de publics)\n' +
    '                                        </li>\n' +
    '                                        <li>Mise en oeuvre de l\'<strong>annuaire régional des CMA</strong> avec <strong>carte\n' +
    '                                            des régions interactive</strong></li>\n' +
    '                                        <li>Mise en place d\'un <strong>hub de services en ligne</strong></li>\n' +
    '                                        <li>Intégration d\'un <strong>solution d\'édition de newsletters et gestion des\n' +
    '                                            listes d\'abonnés</strong> - routage emailing professionnel\n' +
    '                                        </li>\n' +
    '                                        <li>Intégration d\'un <strong>espace Recrutement en ligne</strong> et d\'un\n' +
    '                                            <strong>espace Relations Presse</strong></li>\n' +
    '                                    </ul>\n' +
    '                                    <hr>\n' +
    '                                    <h4>CMS : Drupal 8</h4>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';

let etudetours = '<div class="etude etude-tours">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/tours/logo.png" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">Centre Hospitalier Universitaire de la région Centre–Val de\n' +
    '                                Loire</h2>\n' +
    '                            <a href="https://www.chu-tours.fr/" target="_blank" class="btn btn-etude">Voir le site\n' +
    '                                internet <i class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-lg-5 col-md-6">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p>Le <strong>CHRU de Tours</strong> est l\'unique <strong>Centre Hospitalier\n' +
    '                                        Universitaire de la région Centre–Val de Loire</strong>. Il tient le rôle\n' +
    '                                        essentiel d’établissement <strong>support du Groupement Hospitalier de\n' +
    '                                            Territoire (GHT) Touraine-Val de Loire</strong>.</p>\n' +
    '                                    <p>Il contient 16 pôles d’activités, 1 plateau médicotechnique avec <strong>près de\n' +
    '                                        2 000 lits et places</strong> répartis sur 5 sites. Le <strong>CHRU de\n' +
    '                                        Tours</strong> emploie près de <strong>4 400 soignants</strong>.</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-lg-5 col-md-6">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p>Le nouveau site est le <strong>pilier de la stratégie digitale</strong> du\n' +
    '                                            CHRU Tours. Il soutient la stratégie de marque et porte la <strong>nouvelle\n' +
    '                                                organisation interne de l’établissement</strong>.</p>\n' +
    '\n' +
    '                                        <p>L\'ambition autour du projet est de <strong>coopérer, rassembler,\n' +
    '                                            coordonner</strong> et <strong>mutualiser les moyens, réorganiser,\n' +
    '                                            fédérer</strong> et <strong>avancer dans un même but</strong> en <strong>faisant\n' +
    '                                            évoluer l’offre de soins et de services</strong> et en <strong>favorisant le\n' +
    '                                            recrutement</strong>.</p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-lg-7 col-md-6 conteneur-longpage">\n' +
    '                                <img src="img/etudes/tours/longpage.jpg" alt="" class="longpage">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <img src="img/etudes/tours/mobile.jpg" alt="" class="mobile">\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-8 offset-xl-4">\n' +
    '                                <h3>Notre intervention</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="col-lg-6">\n' +
    '                                            <h4>Nouvelle approche éditoriale :</h4>\n' +
    '                                            <ul>\n' +
    '                                                <li>Arborescence : <strong>entrées par typologies de publics</strong>\n' +
    '                                                </li>\n' +
    '                                                <li><strong>Restructuration de l\'Offre de soins</strong> : accès,\n' +
    '                                                    articulation des pages, lisibilité des Fiches Pôles et Services\n' +
    '                                                </li>\n' +
    '                                                <li>Importante volumétrie de contenus</li>\n' +
    '                                            </ul>\n' +
    '                                        </div>\n' +
    '                                        <div class="col-lg-6">\n' +
    '                                            <h4>Facilitation de l\'accès à l\'offre de soins via un moteur de recherche\n' +
    '                                                central</h4>\n' +
    '                                            <ul>\n' +
    '                                                <li>Annuaires des services et médecins</li>\n' +
    '                                                <li>Recherche visuelle par partie du corps, par maladie ou handicap</li>\n' +
    '                                            </ul>\n' +
    '                                            <p><strong>Formulaires pour prise de rendez-vous en ligne</strong></p>\n' +
    '                                            <p><strong>Espace Recrutement : Intégration du Portail d\'emploi\n' +
    '                                                in-page</strong></p>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <span class="btn btn-etude">CMS : Wordpress</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';

let etudeinhesj = '<div class="etude etude-inhesj">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/inhesj/logo.png" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">Institut national des hautes études de la sécurité et de la\n' +
    '                                justice</h2>\n' +
    '                            <a href="https://inhesj.fr/" target="_blank" class="btn btn-etude">Voir le site internet <i\n' +
    '                                    class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-md-6">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p><strong>L’Institut National des Hautes Études de la Sécurité et de la\n' +
    '                                        Justice</strong> (INHESJ), placé sous l’autorité du <strong>Premier\n' +
    '                                        ministre</strong>, dépend des <strong>Ministères de l’Intérieur et de la\n' +
    '                                        Justice</strong>.</p>\n' +
    '\n' +
    '                                    <p>Il accompagne les politiques publiques en matière de <strong>défense, sécurité,\n' +
    '                                        justice, d’intelligence et de sécurité économique</strong> ou encore de <strong>gestion\n' +
    '                                        des risques et des crises</strong>. Il œuvre par la formation professionnelle,\n' +
    '                                        la recherche, l\'expertise, la diffusion de ressources (ONDRP) et l\'organisation\n' +
    '                                        d\'évènements.</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-md-6">\n' +
    '                                <img src="img/etudes/inhesj/mobile.jpg" alt="" class="img-fluid mobile">\n' +
    '                                <img src="img/etudes/inhesj/mobile-mobile.jpg" alt="" class="img-fluid mobile-mobile">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p>Le site de l\'INHESJ doit permettre une <strong>meilleure visibilité de\n' +
    '                                            l\'Institut et valoriser la diversité de ses activités</strong> auprès de ses\n' +
    '                                            différentes cibles. Il vise à promouvoir ses capacités d\'expertise, <strong>les\n' +
    '                                                formations</strong> proposées aux cadres dirigeants du secteur privé\n' +
    '                                            comme public et à diffuser plus largement les <strong>travaux de recherche\n' +
    '                                                menés</strong>.</p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6 offset-xl-1 conteneur-longpage">\n' +
    '                                <img src="img/etudes/inhesj/longpage.jpg" alt="" class="longpage img-fluid">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-7 offset-xl-5">\n' +
    '                                <h3>Notre intervention</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <ul>\n' +
    '                                        <li><strong>Restructuration éditoriale - réorganisation des contenus</strong>\n' +
    '                                        </li>\n' +
    '                                        <li><strong>Facilitation de l\'accès à l\'offre de services "Formations"\n' +
    '                                            :</strong>\n' +
    '                                            <ul>\n' +
    '                                                <li>Moteur de recherche multicritères (domaines traités, types de\n' +
    '                                                    public, type de formation et durée)\n' +
    '                                                </li>\n' +
    '                                                <li>Prise de contact et inscription aux évènements par formulaires\n' +
    '                                                    complexes\n' +
    '                                                </li>\n' +
    '                                            </ul>\n' +
    '                                        </li>\n' +
    '                                    </ul>\n' +
    '                                    <ul>\n' +
    '                                        <li>\n' +
    '                                            <strong>Intégration d\'un espace de ressources documentaires</strong>\n' +
    '                                            <ul>\n' +
    '                                                <li>Accès à 9 typologies de publications</li>\n' +
    '                                                <li>Moteur de recherche multicritères des publications</li>\n' +
    '                                                <li>Consultation des ressources en ligne</li>\n' +
    '                                                <li>Présentation des projets nationaux et internationaux</li>\n' +
    '                                            </ul>\n' +
    '                                        </li>\n' +
    '                                    </ul>\n' +
    '                                    <span class="btn btn-etude">CMS : Drupal 8</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';

let etudemlp = '<div class="etude etude-mlp">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/mlp/logo.jpg" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">Pour l\'autonomie et l\'insertion professionnelle des jeunes</h2>\n' +
    '                            <a href="https://www.missionlocale.paris/" target="_blank" class="btn btn-etude">Voir le\n' +
    '                                site internet <i class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-lg-6 col-md-12 offset-lg-6 offset-xl-7">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p>La <strong>Mission Locale de Paris</strong> accompagne les jeunes en difficulté\n' +
    '                                        dans leurs <strong>parcours d’insertion professionnelle et sociale</strong> par\n' +
    '                                        un accueil de proximité.</p>\n' +
    '\n' +
    '                                    <p>Une <strong>prise en charge individuelle</strong> permet au jeune de gagner en\n' +
    '                                        autonomie et prendre en main leur carrière professionnelle. Le dispositif repose\n' +
    '                                        sur des ateliers collectifs, des stages en entreprises et un réseau de\n' +
    '                                        partenaires locaux afin de lever tous les freins à l\'insertion.\n' +
    '                                    </p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '                        <img src="img/etudes/mlp/mobile.png" alt="" class="mobile">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-md-6 conteneur-longpage">\n' +
    '                                <img src="img/etudes/mlp/longpage.jpg" alt="" class="longpage">\n' +
    '                            </div>\n' +
    '                            <div class="col-xl-12 col-md-6">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p>Le <strong>portail web unifié</strong> de la MLP constitue l’<strong>outil\n' +
    '                                            digital central de la stratégie renforcée d’accompagnement des jeunes\n' +
    '                                            parisiens en situation de précarité</strong>. Il apporte une <strong>plus-value\n' +
    '                                            fonctionnelle</strong> servant la vision prospective de la MLP.</p>\n' +
    '\n' +
    '                                        <p>Il est constitué d’<strong>une partie publique d’information</strong>,\n' +
    '                                            interface <strong>facilitant l’entrée des jeunes concernés dans le\n' +
    '                                                dispositif</strong>, et de <strong>plusieurs espaces de gestion\n' +
    '                                                personnalisée à accès sécurisé</strong> à la fois pour chaque jeune,\n' +
    '                                            conseiller, et partenaire de la MLP.\n' +
    '                                        </p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '\n' +
    '\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-7 offset-xl-5">\n' +
    '                                <h3>Notre intervention</h3>\n' +
    '                                <div class="description row">\n' +
    '                                    <div class="col-lg-6">\n' +
    '                                        <ul>\n' +
    '                                            <li>\n' +
    '                                                Unification des systèmes hétérogènes en 1 portail digital unique\n' +
    '                                            </li>\n' +
    '                                            <li>\n' +
    '                                                Fluidification des workflows de travail entre les différents secteurs\n' +
    '                                            </li>\n' +
    '                                            <li>\n' +
    '                                                Simplification de l\'accès à l\'information\n' +
    '                                            </li>\n' +
    '                                            <li>\n' +
    '                                                Refonte graphique et ergonomique facilitant l\'accès pour les cibles\n' +
    '                                                jeunes\n' +
    '                                            </li>\n' +
    '                                            <li>\n' +
    '                                                Facilitation de la rentrée dans le dispositif (collecte de candidatures)\n' +
    '                                            </li>\n' +
    '                                        </ul>\n' +
    '                                    </div>\n' +
    '                                    <div class="col-lg-6">\n' +
    '                                        <p><strong>Développement d\'interfaces front-office en fonction des attentes des\n' +
    '                                            différentes cibles :</strong></p>\n' +
    '                                        <ul>\n' +
    '                                            <li>Agendas / Gestion de la programmation, plannings de réservation</li>\n' +
    '                                            <li>Outils de testing</li>\n' +
    '                                            <li>Outil de suivi des jeunes et de leurs démarches professionnelles</li>\n' +
    '                                            <li>Interfaçage avec solution métier i-Milo</li>\n' +
    '                                            <li>Compte jeune : inscription ateliers, gestion agenda, suivi progrès,\n' +
    '                                                outils de contacts\n' +
    '                                            </li>\n' +
    '                                        </ul>\n' +
    '                                    </div>\n' +
    '                                    <div class="col-12">\n' +
    '                                        <span class="btn btn-etude">CMS : WORDPRESS</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';


let etudepdch = '<div class="etude etude-pdch">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/pdch/logo.png" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">Pas de Calais Habitat</h2>\n' +
    '                            <a href="https://www.pasdecalais-habitat.fr/" target="_blank" class="btn btn-etude">Voir le\n' +
    '                                site internet <i class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p><strong>Pas de Calais Habitat</strong> est le <strong>3e Office Public de\n' +
    '                                        l\'Habitat en France</strong> en nombre de logements. Il gère plus <strong>de 40\n' +
    '                                        000 logements</strong> répartis sur 5 territoires / bassins et près de 200\n' +
    '                                        communes.</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-md-6">\n' +
    '                                <img src="img/etudes/pdch/mobile.png" alt="" class="mobile">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p><strong>Bailleur social de référence dans son département,</strong>\n' +
    '                                            Pas-de-Calais habitat souhaite faire évoluer en profondeur sa stratégie de\n' +
    '                                            communication on-line et disposer d\'un <strong>outil web dédié à la relation\n' +
    '                                                commerciale</strong>.</p>\n' +
    '                                        <p>Avec ce site, Pas-de-Calais habitat souhaite maintenir un lien direct avec\n' +
    '                                            ses locataires pour une <strong>meilleure gestion</strong> et prise en\n' +
    '                                            compte de leurs demandes.</p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6 col-xl-6 offset-xl-1 conteneur-longpage">\n' +
    '                                <img src="img/etudes/pdch/longpage.jpg" alt="" class="longpage img-fluid">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-7 offset-xl-5">\n' +
    '                                <h3>Notre intervention</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="col-lg-6">\n' +
    '                                            <p><strong>• Mise à jour en temps réel des données du Parc immobilier\n' +
    '                                                :</strong>\n' +
    '                                                Interfaçage avec la <strong>solution métier Consortium</strong></p>\n' +
    '                                            <p>• <strong>Extranet locataires : 6 000 comptes actifs</strong> - connexion\n' +
    '                                                sécurisée - <strong>Interfaçage bidirectionnel avec le SI\n' +
    '                                                    distant</strong> -\n' +
    '                                                Gestion données clients / Abonnement quittances dématérialisées /\n' +
    '                                                Contacts\n' +
    '                                                personnalisés / Demande d\'intervention / Paiement en ligne via solution\n' +
    '                                                CB\n' +
    '                                                Caisse d\'Épargne / Mise à disposition de documents liés au contrat</p>\n' +
    '                                        </div>\n' +
    '                                        <div class="col-lg-6">\n' +
    '                                            <p>\n' +
    '                                                <strong>Diffusions de l\'offre commerciale en direct :</strong>\n' +
    '                                            <ul>\n' +
    '                                                <li><strong>Recherches multicritères,</strong></li>\n' +
    '                                                <li><strong>Géolocalisation</strong> des biens sur carte</li>\n' +
    '                                                <li>Restructuration des fiches Annonces et partage réseaux sociaux</li>\n' +
    '                                            </ul>\n' +
    '                                            </p>\n' +
    '                                            <p>\n' +
    '                                                <strong>hausse du taux de conversion,</strong> amélioration <strong>de\n' +
    '                                                la relation client :</strong>\n' +
    '                                            <ul>\n' +
    '                                                <li>Alertes</li>\n' +
    '                                                <li>Call to action vers formulaire de contact</li>\n' +
    '                                            </ul>\n' +
    '                                            </p>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <span class="btn btn-etude">CMS : Wordpress</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';

let etudebretagne = '<div class="etude etude-bretagne">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/bretagne/logo.png" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">L\'entreprise en continu avec les CCi de Bretagne</h2>\n' +
    '                            <a href="https://www.bretagne-economique.com/" target="_blank" class="btn btn-etude">Voir le\n' +
    '                                site internet <i class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-lg-6 col-md-7 offset-md-5 offset-lg-6 offset-xl-7">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p>Les <strong>CCI de Bretagne</strong> représentent les intérêts des <strong>124\n' +
    '                                        000 entreprises</strong> du commerce, de l\'industrie et des services de la\n' +
    '                                        région. Les services apportés aux entreprises s\'articulent autour de cinq grands\n' +
    '                                        domaines que sont l\'information, le conseil aux entreprises, la formation, la\n' +
    '                                        gestion d\'infrastructures et l\'aménagemement du territoire.</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-lg-6 col-md-5 conteneur-longpage">\n' +
    '                                <img src="img/etudes/bretagne/longpage.jpg" alt="" class="longpage">\n' +
    '                            </div>\n' +
    '                            <div class="col-xl-6 offset-xl-1 col-lg-6 text-right conteneur-mobile">\n' +
    '                                <img src="img/etudes/bretagne/mobile.jpg" alt="" class="mobile">\n' +
    '                            </div>\n' +
    '                            <div class="col-lg-12 col-lg-6 col-md-7">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p>Dans un contexte d\'<strong>évolution de la carte consulaire</strong> visant à\n' +
    '                                            conserver 4 CCIT et 1 CCIR, les <strong>CCI de Bretagne</strong> se sont\n' +
    '                                            fixées pour objectif de <strong>digitaliser le magazine trimestriel</strong>.\n' +
    '                                            Le site <strong>www.bretagne-economique.com</strong> doit être refondu\n' +
    '                                            entièrement pour s\'enrichir de <strong>nouvelles fonctionnalités</strong>\n' +
    '                                            (bannières publicitaires, newsletter, vidéothèque, paiement sécurisé, ...),\n' +
    '                                            <strong>proposer un accès simple et pratique aux magazines</strong>, et ce\n' +
    '                                            dans un environnement graphique et ergonomique adapté respectant la charte\n' +
    '                                            <strong>CCI France</strong> et adapté aux attentes des entreprises\n' +
    '                                            utilisatrices.</p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '\n' +
    '\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-7 offset-xl-5">\n' +
    '                                <h3>Nos interventions</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <ul>\n' +
    '                                        <li>Approche <strong>ergonomique / UX design</strong></li>\n' +
    '                                        <li>Conseil à la <strong>structuration de l\'arborescence</strong> et des\n' +
    '                                            <strong>modes de navigation</strong></li>\n' +
    '                                        <li><strong>Migration de contenus</strong> (+ de 3 000 pages de l\'ancien site\n' +
    '                                            avec différentes typologies de contenu\n' +
    '                                        </li>\n' +
    '                                        <li><strong>Digitalisation</strong> de supports papier : magazines et\n' +
    '                                            hors-séries\n' +
    '                                        </li>\n' +
    '                                        <li><strong>Maillage intelligent</strong> avec l\'offre de service des CCI de\n' +
    '                                            Bretagne\n' +
    '                                        </li>\n' +
    '                                        <li>Exploitation de <strong>taxonomies avancées</strong> pour qualifier les\n' +
    '                                            articles\n' +
    '                                        </li>\n' +
    '                                        <li><strong>Intégration et optimisation</strong> du moteur de recherche <strong>SoIR</strong>\n' +
    '                                        </li>\n' +
    '                                        <li>Module de gestion de <strong>bannières publicitaires</strong></li>\n' +
    '                                        <li><strong>Achat en ligne</strong> des hors-séries avec paiement sécurisé</li>\n' +
    '                                        <li>Module d\'<strong>édition et routage de Newsletters</strong> thématiques (>\n' +
    '                                            150 000 emails par mois)\n' +
    '                                        </li>\n' +
    '                                    </ul>\n' +
    '                                    <hr>\n' +
    '                                    <span class="btn btn-etude">CMS : DRUPAL 7</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';


let etudeipparis = '<div class="etude etude-ipparis">\n' +
    '                <header class="">\n' +
    '                    <div class="container">\n' +
    '                        <div class="intro text-center">\n' +
    '                            <img src="img/etudes/ipparis/logo.png" alt="" class="logo img-fluid">\n' +
    '                            <h2 class="titre-etude">Cinq grandes Écoles autour d\'une ambition commune</h2>\n' +
    '                            <a href="https://www.ip-paris.fr/" target="_blank" class="btn btn-etude">Voir le\n' +
    '                                site internet <i class="far fa-long-arrow-right"></i></a>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5">\n' +
    '                                <h3>Le contexte</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p>L\'Institut Polytechnique de Paris est un établissement d\'enseignement, de\n' +
    '                                        recherche et d\'innovation bénéficiant de l\'expertise de ses Ecoles fondatrices :\n' +
    '                                        l\'Ecole polytechnique, L\'ENSTA Paris, L\'ENSAE Paris, Télécom Paris et SudParis,\n' +
    '                                        le positionnant comme l\'un des huit plus puissants pôles d\'innovation au\n' +
    '                                        monde.</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </header>\n' +
    '                <section class="projet">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-5 col-lg-6">\n' +
    '                                <img src="img/etudes/ipparis/logos-ipparis.png" alt="" class="logos">\n' +
    '                                <img src="img/etudes/ipparis/mobile.png" alt="" class="mobile">\n' +
    '                                <img src="img/etudes/ipparis/mobile-mobile.png" alt="" class="mobile-mobile">\n' +
    '                                <div class="description-projet">\n' +
    '                                    <h3>Le projet</h3>\n' +
    '                                    <div class="description">\n' +
    '                                        <p>Le nouveau site est envisagé comme un portail d’entrée vers l’univers «\n' +
    '                                            Polytechnique ». Il valorise tant la variété de la formation d’excellence,\n' +
    '                                            la recherche de pointe, que la quête d’innovations multi partenariales. Il\n' +
    '                                            transmet la vision précise de l’excellence française dans les domaines\n' +
    '                                            scientifiques et économiques.</p>\n' +
    '                                        <p>Le site constitue une entrée vers l’offre d’excellence qui correspond à\n' +
    '                                            chaque visiteur avec des objectifs forts de recrutement, au plan national\n' +
    '                                            comme à l’international.</p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-lg-6 offset-xl-1 conteneur-longpage">\n' +
    '                                <img src="img/etudes/ipparis/longpage.jpg" alt="" class="longpage img-fluid">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="intervention">\n' +
    '                    <div class="container">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xl-7 offset-xl-5">\n' +
    '                                <h3>Notre intervention</h3>\n' +
    '                                <div class="description">\n' +
    '                                    <p>\n' +
    '                                    <ul>\n' +
    '                                        <li>Accompagnement</li>\n' +
    '                                        <li>Restructuration de l\'info</li>\n' +
    '                                        <li>Approche user centrix</li>\n' +
    '                                        <li>Charte graphique : une identité forte dans un esprit de séduction assumé</li>\n' +
    '                                        <li>Ergonomie : approche Mobile first et marketing assumée</li>\n' +
    '                                        <li>Usine à sites</li>\n' +
    '                                        <li>Multilinguisme</li>\n' +
    '                                        <li>Moteur de recherche Programmes de formation (Course finder)</li>\n' +
    '                                        <li>Témoignages</li>\n' +
    '                                    </ul>\n' +
    '                                    </p>\n' +
    '                                    <span class="btn btn-etude">CMS : Drupal 8</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '                <section class="cta container">\n' +
    '                    <span class="btn btn-etude gotocontact"><i class="fal fa-envelope"></i>Ce projet vous intéresse, discutons\n' +
    '                        en</span>\n' +
    '                </section>\n' +
    '            </div>';


function nc_captcha_submit(token)
{
	$=jQuery;

	var name 	= $('#contact [name=name]').val();
	var soc		= $('#contact [name=soc]').val();
	var mail 	= $('#contact [name=mail]').val();
	var phone 	= $('#contact [name=phone]').val();
	var message = $('#contact [name=message]').val();
	var captcha = token;

	$.post(
		'/index.php',
		{
			contact: {
				name: name,
				soc: soc,
				mail: mail,
				phone: phone,
				message: message,
				captcha: captcha,
			},
			action: 'contact',
		},
		function(data){
			switch(data.state)
			{
				case 0:
					$('form#contact')[0].reset();
					$('#modal-contact-titre').html(data.titre);
					$('#modal-contact-contenu').html(data.msg);
					$('#modal-contact').removeClass('modal-danger').addClass('modal-success').modal('show');
					break;
					
				case 1:
				case 2:
				case 3:
				default:
					for(field in data.data)
					{
						if($('form#contact [name=' + field + ']').length)
						{
							$('form#contact [name=' + field + ']').val(data.data[field]);
						}
					}
					$('#modal-contact-titre').html(data.titre);
					$('#modal-contact-contenu').html(data.msg);
					$('#modal-contact').removeClass('modal-success').addClass('modal-danger').modal('show');
					break;
			}
		},
		'json'
	);
}








